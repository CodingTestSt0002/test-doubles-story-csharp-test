﻿
namespace TestDoublesStory.Interfaces
{
    public interface IReservationRepository
    {
        bool IsOpenForReservation(long hotelId, DateTime startDate, DateTime endDate);
        void Reserve(long hotelId, DateTime startDate, DateTime endDate);
    }
}