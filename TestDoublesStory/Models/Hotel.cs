﻿using System.Numerics;

namespace TestDoublesStory.Models
{
    public class Hotel : IEquatable<Hotel>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int Rank { get; set; }
        public BigInteger PricePerNight { get; set; }

        public static Builder GetBuilder()
        {
            return new Builder();
        }

        public bool Equals(Hotel other)
        {
            if (other == null) return false;
            return Id == other.Id &&
                   Rank == other.Rank &&
                   Name == other.Name &&
                   City == other.City &&
                   PricePerNight == other.PricePerNight;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Hotel);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, City, Rank, PricePerNight);
        }

        public class Builder
        {
            private readonly Hotel _hotel = new Hotel();

            public Builder Id(long id)
            {
                _hotel.Id = id;
                return this;
            }

            public Builder Name(string name)
            {
                _hotel.Name = name;
                return this;
            }

            public Builder City(string city)
            {
                _hotel.City = city;
                return this;
            }

            public Builder Rank(int rank)
            {
                _hotel.Rank = rank;
                return this;
            }

            public Builder PricePerNight(BigInteger pricePerNight)
            {
                _hotel.PricePerNight = pricePerNight;
                return this;
            }

            public Hotel Build()
            {
                return _hotel;
            }
        }
    }
}
