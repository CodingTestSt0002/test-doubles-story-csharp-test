---
focus: /TestDoublesStory/DefaultReservationService.cs
---
### Test Doubles Story
Welcome to coding story about small hotel reservation service.
This story is the usage of test doubles in order to prepare well-written unit tests.

First let's take a look on project structure to understand what is this
business logic about. We define two interfaces of repositories that we will be used in the logic:
[IHotelRepository](/TestDoublesStory/Interfaces/IHotelRepository.cs), with one `FindHotelsByCity` method,
and [IReservationRepository](/TestDoublesStory/Interfaces/IReservationRepository.cs), with two methods:
`IsOpenForReservation` and `Reserve`. They are interfaces only, as we don't care about their actual implementation.
The main class, that we will test is 
[DefaultReservationService](/TestDoublesStory/DefaultReservationService.cs),
where we define logic for performing reservation of the best hotel in the given city in defined date range.
