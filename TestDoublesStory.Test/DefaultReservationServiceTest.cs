using Moq;
using TestDoublesStory.Interfaces;
using TestDoublesStory.Models;

namespace TestDoublesStory.Test
{
    public class DefaultReservationServiceTest
    {
        private const string City = "Paris";
        private static readonly DateTime StartDate = DateTime.Parse("2022-10-12T20:00:00Z");
        private static readonly DateTime EndDate = DateTime.Parse("2022-10-13T10:00:00Z");
        private const int BestHotelId = 1;
        private const int OtherHotelId = 2;

        [Fact]
        public void ShouldReserveBestHotelWhenAvailable()
        {
            // Arrange
            var hotelRepository = new HotelRepositoryStub();
            var reservationRepositoryMock = new Mock<IReservationRepository>();
            var reservationService = new DefaultReservationService(hotelRepository, reservationRepositoryMock.Object);

            // Act
            reservationService.ReserveBestHotelInCity(City, StartDate, EndDate);
            reservationRepositoryMock.Setup(repo => repo.IsOpenForReservation(BestHotelId, StartDate, EndDate)).Returns(false);
            reservationRepositoryMock.Setup(repo => repo.IsOpenForReservation(OtherHotelId, StartDate, EndDate)).Returns(true);

            // Assert
            reservationRepositoryMock.Verify(repo => repo.Reserve(OtherHotelId, StartDate, EndDate), Times.Once);
        }

        private class HotelRepositoryStub : IHotelRepository
        {
            private readonly Hotel _bestHotel = new Hotel
            {
                Id = BestHotelId,
                Rank = 1
            };

            private readonly Hotel _otherHotel = new Hotel
            {
                Id = OtherHotelId,
                Rank = 2
            };

            public List<Hotel> FindHotelsByCity(string city)
            {
                return new List<Hotel> { _bestHotel, _otherHotel };
            }
        }
    }
}